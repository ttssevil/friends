import React from 'react'
import Enzyme, { mount } from 'enzyme'
import sinon from 'sinon'
import Adapter from 'enzyme-adapter-react-16'
import App from './App'
import List from './list/List'
import Select from './select/Select'

Enzyme.configure({ adapter: new Adapter() })

describe('<App/>', () => {
  sinon.spy(App.prototype, 'componentDidMount')
  const wrapper = mount(<App />)

  it('calls componentDidMount', () => {
    expect(App.prototype.componentDidMount).toHaveProperty('callCount', 1)
  })

  it('renders <List/> component', () => {
    expect(wrapper.find(List).length).toEqual(1)
  })

  it('renders <Select/> component', () => {
    expect(wrapper.find(Select).length).toEqual(1)
  })
})
