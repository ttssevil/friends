import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import Select from './Select'

Enzyme.configure({ adapter: new Adapter() })

describe('<Select />', () => {
  const data = [
    { id: 1, name: 'name1', friends: [2, 3] },
    { id: 2, name: 'name2', friends: [1] },
    { id: 3, name: 'name3', friends: [2] },
  ]
  const onOptionClick = sinon.spy()
  const wrapper = mount(
    <Select label="test label" data={data} selectedOption={2} onOptionClick={onOptionClick} />,
  )

  it('renders 4 option', () => {
    const option = wrapper.find('option')
    expect(option.length).toEqual(4)
  })

  it('renders label ', () => {
    const label = wrapper.find('h2').length
    expect(label).toEqual(1)
    expect(wrapper.props().label).toEqual('test label')
  })

  it('simulate onChange event', () => {
    wrapper.find('select').simulate('change')
    expect(onOptionClick).toHaveProperty('callCount', 1)
  })
})
