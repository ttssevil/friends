import React from 'react'
import PropTypes from 'prop-types'

const Select = (props) => {
  const {
    data, selectedOption, label, onOptionClick,
  } = props
  return (
    <div>
      <h2>{label}</h2>
      <select onChange={onOptionClick} value={selectedOption}>
        <option value="" disabled>Users</option>
        { data.map(user => <option value={user.id} key={user.id}>{user.name}</option>)}
      </select>
    </div>
  )
}

Select.propTypes = {
  data: PropTypes.instanceOf(Array),
  label: PropTypes.string,
  selectedOption: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onOptionClick: PropTypes.func.isRequired,
}

Select.defaultProps = {
  label: '',
  selectedOption: '',
  data: [],
}

export default Select
