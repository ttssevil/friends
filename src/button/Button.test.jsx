import React from 'react'
import Enzyme, { mount } from 'enzyme'
import sinon from 'sinon'
import Adapter from 'enzyme-adapter-react-16'
import Button from './Button'

Enzyme.configure({ adapter: new Adapter() })

describe('<Button />', () => {
  const onClick = sinon.spy()
  const wrapper = mount(<Button label="button" onClick={onClick} />)

  it('onClick event', () => {
    wrapper.find('button').simulate('click')
    expect(onClick).toHaveProperty('callCount', 1)
  })

  it('render button label', () => {
    expect(wrapper.props().label).toEqual('button')
  })
})
