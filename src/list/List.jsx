import React from 'react'
import PropTypes from 'prop-types'


const List = (props) => {
  const { data, label } = props
  return (
    <div>
      <h1>{label}</h1>
      <ul>
        { data.map(item => (
          <li key={item.id}>
            {item.name}
          </li>
        ))}
      </ul>
    </div>
  )
}

List.propTypes = {
  data: PropTypes.instanceOf(Array),
  label: PropTypes.string,
}

List.defaultProps = {
  data: [],
  label: '',
}


export default List
