import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import List from './List'

Enzyme.configure({ adapter: new Adapter() })

describe('<List />', () => {
  const data = [
    { id: 1, name: 'name1', friends: [2, 3] },
    { id: 2, name: 'name2', friends: [1] },
    { id: 3, name: 'name3', friends: [2] },
  ]
  const wrapper = mount(
    <List label="test label" data={data} />,
  )

  it('renders three li', () => {
    const li = wrapper.find('li')
    expect(li.length).toEqual(3)
  })

  it('renders label ', () => {
    const label = wrapper.find('h1').length
    expect(label).toEqual(1)
    expect(wrapper.props().label).toEqual('test label')
  })
})
