import React, { Component } from 'react'
import config from './firebase/firebase'
import Button from './button/Button'
import Select from './select/Select'
import List from './list/List'
import './App.css'

class App extends Component {
  static createUsersObj(userData) {
    const users = {}
    userData.forEach((user) => { users[user.id] = { name: user.name, friends: user.friends } })
    return users
  }

  constructor(props) {
    super(props)
    this.handleUserChange = this.handleUserChange.bind(this)
    this.handleDeleteUser = this.handleDeleteUser.bind(this)
    this.state = { friendList: [], userData: [], users: {} }
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData() {
    fetch(`${config.databaseURL}/users.json`)
      .then(result => result.json())
      .then((snapshot) => {
        snapshot = snapshot || {} // in case there are not any record in firebase db it returns null
        const userData = []
        Object.keys(snapshot).forEach((id) => {
          const childKey = id
          const childData = snapshot[id]
          const { friends } = childData
          const data = {
            name: childData.name,
            friends: friends ? Object.assign({}, friends) : {},
            id: childKey,
          }
          userData.push(data)
        })
        this.setState({
          userData, users: App.createUsersObj(userData), id: '', friendList: [],
        })
      })
  }


  handleUserChange(e) {
    const id = e.nativeEvent.target.value
    this.setState({ id, friendList: [] }, () => this.findFriends(id))
  }

  findFriends(userId) {
    const { users, id, friendList } = this.state
    const friendsArray = Object.values(users[userId].friends).filter(friend => friend)
    // ids deleted from users friend obj stored as null in firebase, so need to get only truthy ids
    friendsArray.forEach((friendId) => {
      if (friendList.indexOf(friendId) === -1 && friendId !== id) {
        // if not in friendList yet and id different from selected user's
        friendList.push(friendId)
        this.setState({ friendList })
        this.findFriends(friendId)
      }
    })
  }

  handleDeleteUser() {
    const updates = []
    const { userData } = this.state
    userData.forEach((user) => {
      const { id } = this.state
      const { friends } = user
      const currentUserId = user.id
      if (id === currentUserId) updates.push(`${user.id}`)
      // if selected options id , delete entire node
      else if (Object.values(friends).indexOf(id) > -1) {
      // else if node has friend reference to selected option , get reference index from firebase db
      // and delete only that reference
        const index = Object.keys(friends).find(index => friends[index] === id)
        updates.push(`${user.id}/friends/${index}`)
      }
    })
    Promise.all(
      updates.map(url => fetch(`${config.databaseURL}/users/${url}.json`, { method: 'DELETE' })), // bulk delete nodes
    )
      .then(() => this.fetchData()) // get records after update
  }

  render() {
    const {
      users, id, userData, friendList,
    } = this.state
    return (
      <div className="App">
        <Select data={userData} onOptionClick={this.handleUserChange} selectedOption={id} label="Users list:" />
        { id && <Button label={`delete ${users[id].name}`} onClick={this.handleDeleteUser} /> }
        <List
          data={userData.filter(user => friendList.indexOf(user.id) > -1)}
          label={id ? `${users[id].name}'s friends list:` : ''}
        />

      </div>
    )
  }
}

export default App
