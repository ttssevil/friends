module.exports = {
    'env': {
      'browser': true,
      'jest': true,
      'es6': true,
      'node': true,
    },
    'extends': [
      'airbnb',
    ],
    "parser": "babel-eslint" ,
    'parserOptions': {
      'ecmaFeatures': {
        'jsx': true,
      }
    } , 
    'rules': {
        "indent": 2, 
        "no-tabs": 0 , 
        "linebreak-style": [
            "warn",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "no-shadow": "off",
        "semi": [
            "warn",
            "never"
        ] , 
        "react/sort-comp": ["warn", {
            "order": [
              "static-methods",
              "lifecycle",
              "everything-else",
              "render"
            ]}
        ] , 
        "react/prop-types": [2] ,
        "no-param-reassign": 0 , 
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }]
    }
  }